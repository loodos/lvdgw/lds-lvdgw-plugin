# Loodos Vanilla Delivery Gateway - Plugin

[![License](https://img.shields.io/badge/License-Apache%202.0-blue.svg?style=flat-square)](https://gitlab.com/loodos/lvdgw/lds-lvdgw-plugin/blob/master/LICENSE)

[![dev build status](https://gitlab.com/loodos/lvdgw/lds-lvdgw-plugin/badges/dev/build.svg)](https://gitlab.com/loodos/lvdgw/lds-lvdgw-plugin/commits/dev)
[![master build status](https://gitlab.com/loodos/lvdgw/lds-lvdgw-plugin/badges/master/build.svg)](https://gitlab.com/loodos/lvdgw/lds-lvdgw-plugin/commits/master)

[![NuGet Pre Release](https://img.shields.io/nuget/v/LDS.LVDGW.Plugin.svg)](https://www.nuget.org/packages/LDS.LVDGW.Plugin/)
[![NuGet Pre Release](https://img.shields.io/nuget/vpre/LDS.LVDGW.Plugin.svg)](https://www.nuget.org/packages/LDS.LVDGW.Plugin/)

## What is it

Interface library for developing a plugin to LVDGW

## Developing a plugin

//TODO: write description

### Create the project

//TODO: write description

### Add Dependencies

//TODO: write description

### Create the Plugin

//TODO: write description

## Full Sample

//TODO: write description

## Who is using this

//TODO: add projects which is using this

## Commercial Support

//TODO: write description

## Contributing

//TODO: write description

## License

Loodos Vanilla Delivery Gateway (LVDGW) is release under Apache 2.0 License.

Copyright (c) 2018 [Loodos](https://loodos.com)

See [LICENSE](https://loodos.com)
//TODO: add licence file link

## Acknowledgements

- [ASP.NET Core](https://github.com/aspnet)

Thanks for providing free open source licensing

- [Serilog](https://github.com/serilog)
