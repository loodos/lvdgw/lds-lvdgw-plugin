﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace LDS.LVDGW.Plugin.ResponseWrapper
{
    public interface IResponseWrapperPlugin : IPlugin
    {
        Task<string> WrapResponseAsync(string responseBody, int statusCode, string contentType);
    }
}
