﻿using Microsoft.Extensions.Primitives;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace LDS.LVDGW.Plugin.Microservice
{
    public class MicroserviceResponse
    {
        public int StatusCode { get; set; }
        public string ContentType { get; set; }
        public IDictionary<string, StringValues> Headers { get; set;  }
        public string Body { get; set; }

        public byte[] BodyBytes
        {
            get {
                return Encoding.UTF8.GetBytes(this.Body);
            }
        }
    }
}
