﻿using Microsoft.Extensions.Primitives;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace LDS.LVDGW.Plugin.UserAuthentication
{
    public interface IUserAuthenticationPlugin : IPlugin
    {
        Task<bool> ValidateTokenAsync(string token);
        Task<IDictionary<string, object>> ValidateAndRetrieveClaimsAsync(string token);
    }
}
