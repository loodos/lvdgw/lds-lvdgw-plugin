﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LDS.LVDGW.Plugin.UserAuthentication
{
    public class TokenExpiredException: Exception
    {
        public DateTime? Expiration { get; internal set; }

        public TokenExpiredException(string message, DateTime? expiration): base(message)
        {
            Expiration = expiration;
        }
    }
}
