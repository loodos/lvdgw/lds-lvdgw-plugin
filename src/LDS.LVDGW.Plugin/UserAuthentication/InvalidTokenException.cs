﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LDS.LVDGW.Plugin.UserAuthentication
{
    public class InvalidTokenException : Exception
    {
        public InvalidTokenException(string message): base(message)
        {
        }
    }
}
