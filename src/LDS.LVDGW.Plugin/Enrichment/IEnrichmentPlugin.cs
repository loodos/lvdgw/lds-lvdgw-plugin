﻿using Microsoft.Extensions.Primitives;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace LDS.LVDGW.Plugin.Enrichment
{
    public interface IEnrichmentPlugin: IPlugin
    {
        Task EnrichAsync(HttpContext httpContext);
    }
}
