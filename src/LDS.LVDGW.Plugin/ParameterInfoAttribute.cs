﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LDS.LVDGW.Plugin
{
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    public class ParameterInfoAttribute : Attribute
    {
        public string Key { get; set; }
        public string Description { get; set; }
        public object DefaultValue { get; set; }
        public object SampleValue { get; set; }
        public ParameterTypes ParameterType { get; set; } = ParameterTypes.Text;
        public bool Mandatory { get; set; } = false;

        public ParameterInfoAttribute(string key, string description)
        {
            Key = key;
            Description = description;
        }
    }
}
