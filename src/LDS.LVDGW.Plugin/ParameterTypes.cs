﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LDS.LVDGW.Plugin
{
    public enum ParameterTypes
    {
        Text,
        Url,
        Number,
        Boolean
    }
}
