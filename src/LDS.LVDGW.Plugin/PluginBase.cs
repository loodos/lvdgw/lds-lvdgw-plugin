﻿using Microsoft.Extensions.DependencyInjection;
using Serilog;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace LDS.LVDGW.Plugin
{
    public abstract class PluginBase : IPlugin
    {
        /// <summary>
        /// Plugin name definitation in config file.
        /// It is defined on runtime from assembly name.
        /// You can override it.
        /// </summary>
        public virtual string Name => this.GetType().Assembly.GetName().Name.Replace("LDS.LVDGW.Plugin.", "");

        public string InstanceName { get; set; }
        public ILogger Log { get; set; }

        public List<ParameterInfoAttribute> GetParameters()
        {
            return this.GetType().GetProperties().Where(x => Attribute.IsDefined(x, typeof(ParameterInfoAttribute)))
                .Select(x => x.GetCustomAttribute<ParameterInfoAttribute>()).ToList();
        }

        public void SetAndValidateParameters(IDictionary<string, object> parameters)
        {
            var props = this.GetType().GetProperties().Where(x => Attribute.IsDefined(x, typeof(ParameterInfoAttribute)));

            foreach (PropertyInfo prop in props)
            {
                var parameterInfoAttribute = prop.GetCustomAttribute<ParameterInfoAttribute>();

                object value = parameterInfoAttribute.DefaultValue;
                if (parameters != null && parameters.ContainsKey(parameterInfoAttribute.Key))
                    value = parameters[parameterInfoAttribute.Key];

                if (parameterInfoAttribute.Mandatory && value == null)
                {
                    throw new ArgumentNullException($"{this.Name}:{parameterInfoAttribute.Key} parameter not defined.");
                }

                if (value != null)
                {
                    string strValue = value.ToString();

                    if (parameterInfoAttribute.ParameterType == ParameterTypes.Boolean)
                    {
                        if (!Boolean.TryParse(strValue, out bool result))
                            throw new FormatException($"{this.Name}:{parameterInfoAttribute.Key} parameter must be boolean => {strValue}");

                        prop.SetValue(this, result);
                    }
                    else if (parameterInfoAttribute.ParameterType == ParameterTypes.Number)
                    {
                        if (!Int32.TryParse(strValue, out int result))
                            throw new FormatException($"{this.Name}:{parameterInfoAttribute.Key} parameter must be integer => {strValue}");

                        prop.SetValue(this, result);
                    }
                    else if (parameterInfoAttribute.ParameterType == ParameterTypes.Url)
                    {
                        if (!Uri.TryCreate(strValue, UriKind.Absolute, out Uri result)
                            && (result.Scheme == Uri.UriSchemeHttp || result.Scheme == Uri.UriSchemeHttps))
                            throw new FormatException($"{this.Name}:{parameterInfoAttribute.Key} parameter must be uri => {strValue}");

                        prop.SetValue(this, strValue);
                    }
                    else if (parameterInfoAttribute.ParameterType == ParameterTypes.Text)
                    {
                        if (string.IsNullOrWhiteSpace(strValue))
                            throw new FormatException($"{this.Name}:{parameterInfoAttribute.Key} parameter must be non empty string => {strValue}");

                        prop.SetValue(this, strValue);
                    }
                }
            }
        }

        public virtual void ConfigureServices(IServiceCollection services)
        {
            // do nothing at this leve, can be override in plugin.
        }
    }
}
