﻿using Microsoft.Extensions.DependencyInjection;
using Serilog;
using System;
using System.Collections.Generic;

namespace LDS.LVDGW.Plugin
{
    public interface IPlugin
    {
        /// <summary>
        /// Gets the name of the plugin.
        /// </summary>
        string Name { get; }

        string InstanceName { get; set; }
        ILogger Log { get; set; }

        List<ParameterInfoAttribute> GetParameters();

        void SetAndValidateParameters(IDictionary<string, object> parameters);
        void ConfigureServices(IServiceCollection services);
    }
}
